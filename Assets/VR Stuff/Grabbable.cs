﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grabbable : MonoBehaviour
{

	public Transform controllerLeft, controllerRight;
	bool grabbedLeft, grabbedRight;
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown ("joystick button 14")) { //left
			if ((transform.position - controllerLeft.position).magnitude < 1) {
				grabbedLeft = true;
			}
		}
		if (Input.GetKeyDown ("joystick button 15")) { //right
			if ((transform.position - controllerRight.position).magnitude < 1) {
				grabbedRight = true;
			}
		}

		if (Input.GetKeyUp ("joystick button 14")) { //left
			grabbedLeft = false;
		}

		if (Input.GetKeyUp ("joystick button 15")) { //right
			grabbedRight = false;
		}

		if (grabbedLeft)
			transform.position = controllerLeft.position;
		if (grabbedRight)
			transform.position = controllerRight.position;
	}
}
