﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Handle : MonoBehaviour
{

	public Transform controllerLeft, controllerRight;

	public Slider slider;
	MeshRenderer sliderMesh;
	bool grabbedLeft, grabbedRight;

	void Start ()
	{
		sliderMesh = slider.gameObject.GetComponent<MeshRenderer> ();
	}

	void Update ()
	{
		//Debug.Log (Input.GetAxis ("Horizontal") + " " + Input.GetAxis ("Vertical"));
		if (Input.GetKeyDown ("joystick button 14")) { //left
			if ((transform.position - controllerLeft.position).magnitude < 1) {
				grabbedLeft = true;
			}
		}
		if (Input.GetKeyDown ("joystick button 15")) { //right
			if ((transform.position - controllerRight.position).magnitude < 1) {
				grabbedRight = true;
			}
		}
		if (grabbedLeft) {
			if (controllerLeft.position.y < slider.transform.position.y + sliderMesh.bounds.extents.y &&
			    controllerLeft.position.y > slider.transform.position.y - sliderMesh.bounds.extents.y)
				transform.position = new Vector3 (transform.position.x, controllerLeft.position.y, transform.position.z);
		}
		if (grabbedRight) {
			if (controllerRight.position.y < slider.transform.position.y + sliderMesh.bounds.extents.y &&
			    controllerRight.position.y > slider.transform.position.y - sliderMesh.bounds.extents.y)
				transform.position = new Vector3 (transform.position.x, controllerRight.position.y, transform.position.z);
		}
		if (Input.GetKeyUp ("joystick button 14")) { //left
			grabbedLeft = false;
		}

		if (Input.GetKeyUp ("joystick button 15")) { //right
			grabbedRight = false;
		}
	}

}