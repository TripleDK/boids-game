﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BoidManager : MonoBehaviour {

	public GameObject boidGO;
	public int numOfBoids = 0;
	public float separationSpeed, cohesionSpeed, alignmentSpeed, goalSpeed, separationDistance, maxSpeed;
	public int flockSize;
	public static BoidManager boidMan;
	public List<Boid> allBoids = new List<Boid> ();

	void Awake () {
		if (boidMan == null)
			boidMan = this;
		else
			Debug.Log ("Two boid managers found, abort :(");
	}

	// Use this for initialization
	void Start () {
		
		foreach (GameObject boid in GameObject.FindGameObjectsWithTag ("Boid")) {
			allBoids.Add (boid.GetComponent <Boid> ());
		}
		numOfBoids = allBoids.Count;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Space)) {
			allBoids.Add (Instantiate (boidGO, new Vector3 (Random.Range (-5, 5), Random.Range (-5, 5), Random.Range (-5, 5)), Quaternion.identity).GetComponent <Boid> ());
			Boid boid = allBoids [allBoids.Count - 1];
			boid.separationSpeed = separationSpeed;
			boid.cohesionSpeed = cohesionSpeed;
			boid.alignmentSpeed = alignmentSpeed;
			boid.goalSpeed = goalSpeed;
			boid.separationDistance = separationDistance;
			boid.maxSpeed = maxSpeed;
			boid.flockSize = flockSize;
			numOfBoids++;
		}

	}

	public	void UpdateValues () {
		foreach (Boid boid in allBoids) {
			boid.separationSpeed = separationSpeed;
			boid.cohesionSpeed = cohesionSpeed;
			boid.alignmentSpeed = alignmentSpeed;
			boid.goalSpeed = goalSpeed;
			boid.separationDistance = separationDistance;
			boid.maxSpeed = maxSpeed;
			boid.flockSize = flockSize;
		}
	}
}
