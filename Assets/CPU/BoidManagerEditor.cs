﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(BoidManager))]

public class BoidManagerEditor : Editor {

	SerializedProperty separationSpeed, cohesionSpeed, alignmentSpeed, goalSpeed, separationDistance, maxSpeed, flockSize;


	void OnEnable () {
		goalSpeed = serializedObject.FindProperty ("goalSpeed");
		separationSpeed = serializedObject.FindProperty ("separationSpeed");
		alignmentSpeed = serializedObject.FindProperty ("alignmentSpeed");
		cohesionSpeed = serializedObject.FindProperty ("cohesionSpeed");
		separationDistance = serializedObject.FindProperty ("separationDistance");
		maxSpeed = serializedObject.FindProperty ("maxSpeed");
		flockSize = serializedObject.FindProperty ("flockSize");
	}

	public override void OnInspectorGUI () {
		if (GUILayout.Button ("Update variables")) {
			BoidManager boidMan =	(BoidManager)target;
			boidMan.UpdateValues ();
		}
		serializedObject.Update ();
		EditorGUILayout.PropertyField (goalSpeed);
		EditorGUILayout.PropertyField (separationSpeed);
		EditorGUILayout.PropertyField (alignmentSpeed);
		EditorGUILayout.PropertyField (cohesionSpeed);
		EditorGUILayout.PropertyField (separationDistance);
		EditorGUILayout.PropertyField (maxSpeed);
		EditorGUILayout.PropertyField (flockSize);
		serializedObject.ApplyModifiedProperties ();

	}
}
