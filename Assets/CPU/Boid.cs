﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boid : MonoBehaviour {



	public BoidManager boidMan;
	public List<Boid> neighbourBoids = new List<Boid> ();
	public Vector3 velocity;
	public int flockSize = 5;
	public float separationDistance = 0.5f;
	public float separationSpeed, alignmentSpeed, cohesionSpeed, goalSpeed = 1;
	public float maxSpeed = 3;
	public Transform goal;


	void Start () {
		
		goal = GameObject.Find ("Goal").transform;
		velocity = new Vector3 (0, 0, 1);
		boidMan = BoidManager.boidMan;
	}

	void Update () {

		FindLocalBoids (flockSize);

		Separation ();
		Alignment ();
		Cohesion ();
		Goal ();
		ClampVelocity ();
		//	LookAtVel ();
		transform.position += velocity * Time.deltaTime;
	}

	void ClampVelocity () {
		if (velocity.magnitude > maxSpeed) {
			velocity = maxSpeed * velocity / velocity.magnitude;
		}
	}

	void LookAtVel () {
		transform.LookAt (transform.position + velocity);
		transform.RotateAround (transform.position, transform.right, 90);
	}

	public void FindLocalBoids (float flockSize) {
		neighbourBoids.Clear ();
		for (int i = 0; i < boidMan.allBoids.Count; i++) {
			if ((boidMan.allBoids [i].transform.position - transform.position).magnitude <= flockSize) {
				if (boidMan.allBoids [i] != this)
					neighbourBoids.Add (boidMan.allBoids [i]);
			}
		}
	}

	public void Separation () { 
		Vector3 separation = new Vector3 (0, 0, 0); 
		for (int i = 0; i < neighbourBoids.Count; i++) {
			float distance = (transform.position - neighbourBoids [i].transform.position).magnitude;
			if (distance < separationDistance)
				separation += (transform.position - neighbourBoids [i].transform.position); 
			if (distance <= 0.01f) {
				//Debug.Log ("Basically on top of each other");
				separation += new Vector3 (Random.Range (5, 10), Random.Range (5, 10), Random.Range (5, 10));
			}
		}
		Debug.DrawRay (transform.position, separation, Color.red);
		velocity += separation * separationSpeed;
	}


	public void Alignment () {
		Vector3 alignment = new Vector3 (0, 0, 0);
		if (neighbourBoids.Count != 0) {
			for (int i = 0; i < neighbourBoids.Count; i++) {
				alignment += neighbourBoids [i].velocity;
			}
			alignment = (alignment) / neighbourBoids.Count;
		}
		Debug.DrawRay (transform.position, alignment, Color.yellow);
		velocity += alignment * alignmentSpeed / 8;
	}

	public void Cohesion () {
		Vector3 cohesion = new Vector3 (0, 0, 0);
		if (neighbourBoids.Count != 0) {
			for (int i = 0; i < neighbourBoids.Count; i++) {
				cohesion += neighbourBoids [i].transform.position;
			}
			Debug.DrawRay (transform.position, (cohesion / neighbourBoids.Count) - transform.position, Color.blue);
			cohesion = ((cohesion / neighbourBoids.Count) - transform.position) / 100;
		}
		velocity += cohesion * cohesionSpeed;
	}

	public void Goal () {
		Vector3 goalMovement = new Vector3 (0, 0, 0);
		goalMovement = goal.position - transform.position;
		velocity += goalMovement * goalSpeed;
	}
}
