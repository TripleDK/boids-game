﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Camera))]
[ExecuteInEditMode]

public class CallImageEffect : MonoBehaviour {
	public Material material;

	// Use this for initialization
	void Start () {
		if (!SystemInfo.supportsImageEffects || null == material || null == material.shader || !material.shader.isSupported) {
			enabled = false;
			return;
		}
	}

	void OnRenderImage (RenderTexture source, RenderTexture destination) {
		Graphics.Blit (source, destination, material);
	}
}
