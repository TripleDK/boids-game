using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CSBoid : MonoBehaviour {

	public ComputeShader shader;
	public int maxBoids = 8;
	public int gridSize = 1;
	public int threadGroupSize = 8;
	public Vector3 gridDimensions = new Vector3 (10, 10, 10);
	public bool gridNumbers;
	public int[] gridCounter;
	public int[] cumulativeCount;
	public Boid[] allBoidsGPU;
	Boid[] sortedBoids;
	public float flockSize = 5;
	public float separationDistance = 1;
	public float separationSpeed = 1;
	public float alighnmentSpeed = 1;
	public float cohesionSpeed = 1;
	public float goalSpeed = 1;
	public float maxSpeed = 3;
	public Transform[] allBoids;
	public GameObject boid;
	public GameObject gridNum;
	public Transform goal;

	public 	struct Boid {
		public	Vector3 velocity;
		public	Vector3 position;
		public int bin;
	};



	// Use this for initialization
	void Start () {
		allBoidsGPU = new Boid[maxBoids];
		sortedBoids = new Boid[maxBoids];
		allBoids = new Transform[maxBoids];
		for (int i = 0; i < maxBoids; i++) {
			allBoidsGPU [i].position = new Vector3 (Random.Range (0, gridDimensions.x), Random.Range (0, gridDimensions.y), Random.Range (0, gridDimensions.z));
			allBoidsGPU [i].velocity = new Vector3 (Random.Range (0, 100) / 100f, Random.Range (0, 100) / 100f, Random.Range (0, 100) / 100f);
			//allBoidsGPU [i].velocity = new Vector3 (1, 1, 1);
			allBoids [i] = ((GameObject)Instantiate (boid, transform.position, Quaternion.identity)).transform;
			sortedBoids [i].bin = -1;
		}
		gridCounter = new int[(int)gridDimensions.x / gridSize * (int)gridDimensions.y / gridSize * (int)gridDimensions.z / gridSize];
		if (gridNumbers) {
			for (int i = 0; i < gridCounter.Length; i++) {
				GameObject tempNum = (GameObject)Instantiate (gridNum,
					                     new Vector3 (i % gridDimensions.x,
						                     Mathf.Floor ((i % (gridDimensions.x * gridDimensions.y)) / (gridDimensions.x)),
						                     Mathf.Floor (i / (gridDimensions.x * gridDimensions.y)))
					                     + new Vector3 (0.5f, 0.5f, 0.5f),
					                     Quaternion.identity);
				tempNum.GetComponent<TextMeshPro> ().text = "" + i;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			Debug.Log ("Pressing space");

		}


		SetPositions ();
		GridIndex ();
		//	Rotate ();

		RunBinCount ();

		CountingSort ();

		RunCS ();

		RunMovement ();
//		TestFunction();
	}

	void TestFunction () {
		for (int i = 0; i < allBoidsGPU.Length; i++) {
			Debug.Log ("i");
		}
	}

	void RunBinCount () {
		int binCountKernel = shader.FindKernel ("BinCount");

		ComputeBuffer binCounts = new ComputeBuffer (gridCounter.Length, sizeof(int));
		binCounts.SetData (gridCounter);
		ComputeBuffer buffer = new ComputeBuffer (allBoidsGPU.Length, 24 + sizeof(int)); //(Length, size of each element in array);
		buffer.SetData (allBoidsGPU);
		shader.SetBuffer (binCountKernel, "boids", buffer);
		shader.SetBuffer (binCountKernel, "binCount", binCounts);
		shader.SetVector ("gridDimensions", gridDimensions);
		shader.SetInt ("numOfBoids", maxBoids);
		shader.SetInt ("threadGroupSize", threadGroupSize);
		shader.Dispatch (binCountKernel, allBoidsGPU.Length + threadGroupSize - 1, 1, 1);
		binCounts.GetData (gridCounter);
		buffer.GetData (allBoidsGPU);
		binCounts.Dispose ();
		buffer.Dispose ();
	}

	void RunCountSort () {
		int countSortKernel = shader.FindKernel ("CountSort");


	}

	void RunCS () {
		int kernelHandle = shader.FindKernel ("CSMain");
		RenderTexture tex = new RenderTexture (512, 512, 24);
		tex.enableRandomWrite = true;
		tex.Create (); 								//Size was 24 before bin was added
		ComputeBuffer buffer = new ComputeBuffer (allBoidsGPU.Length, 24 + sizeof(int)); //(Length, size of each element in array);
		buffer.SetData (sortedBoids);
		ComputeBuffer binCounts = new ComputeBuffer (gridCounter.Length, sizeof(int));
		binCounts.SetData (gridCounter);
		ComputeBuffer cumulCount = new ComputeBuffer (cumulativeCount.Length, sizeof(int));
		cumulCount.SetData (cumulativeCount);
//		shader.SetTexture (kernelHandle, "Result", tex);
		//shader.Dispatch (kernelHandle, 512 / 8, 512 / 8, 1);	

		shader.SetBuffer (kernelHandle, "boids", buffer);
		shader.SetBuffer (kernelHandle, "binCount", binCounts);
		shader.SetBuffer (kernelHandle, "cumulativeCount", cumulCount);
		shader.SetFloat ("deltaTime", Time.deltaTime);
		shader.SetFloat ("flockSize", flockSize);
		shader.SetFloat ("separationDistance", separationDistance);
		shader.SetFloat ("separationSpeed", separationSpeed);
		shader.SetFloat ("alignmentSpeed", alighnmentSpeed);
		shader.SetFloat ("cohesionSpeed", cohesionSpeed);
		shader.SetFloat ("goalSpeed", goalSpeed);
		shader.SetFloat ("maxSpeed", maxSpeed);
		if (goal != null) {
			shader.SetVector ("goalPos", goal.position);
			shader.SetBool ("goalActive", true);
		} else
			shader.SetBool ("goalActive", false);
		shader.SetInt ("threadGroupSize", threadGroupSize);
		shader.SetVector ("gridDimensions", gridDimensions);
		shader.Dispatch (kernelHandle, sortedBoids.Length + threadGroupSize - 1, 1, 1);
		buffer.GetData (sortedBoids); //Updates allBoidsPos from GPU
		binCounts.Dispose ();
		buffer.Dispose ();
		cumulCount.Dispose ();

	}

	void RunMovement () {
		int kernelHandle = shader.FindKernel ("ApplyVelocity");
		ComputeBuffer buffer = new ComputeBuffer (allBoidsGPU.Length, 24 + sizeof(int)); //(Length, size of each element in array);
		buffer.SetData (sortedBoids);
		ComputeBuffer binCounts = new ComputeBuffer (gridCounter.Length, sizeof(int));
		binCounts.SetData (gridCounter);
		ComputeBuffer cumulCount = new ComputeBuffer (cumulativeCount.Length, sizeof(int));
		cumulCount.SetData (cumulativeCount);
		shader.SetBuffer (kernelHandle, "boids", buffer);
		shader.SetBuffer (kernelHandle, "binCount", binCounts);
		shader.SetBuffer (kernelHandle, "cumulativeCount", cumulCount);


		shader.Dispatch (kernelHandle, sortedBoids.Length + threadGroupSize - 1, 1, 1);
		buffer.GetData (sortedBoids); //Updates allBoidsPos from GPU, I think
		binCounts.Dispose ();
		buffer.Dispose ();
		cumulCount.Dispose ();
	}

	public void GridIndex () {

		for (int i = 0; i < gridCounter.Length; i++) {
			gridCounter [i] = 0;
		}

		for (int i = 0; i < allBoidsGPU.Length; i++) {
			
			//gridCounter [(int)((int)allBoidsGPU [i].position.x) / gridSize, (int)((int)allBoidsGPU [i].position.y) / gridSize, (int)((int)allBoidsGPU [i].position.z) / gridSize]++;
			//		allBoidsGPU [i].bin = 
			//		(int)allBoidsGPU [i].position.x + 
			//		(int)allBoidsGPU [i].position.y * (int)gridDimensions.x +
			//		(int)allBoidsGPU [i].position.z * (int)gridDimensions.x * (int)gridDimensions.y;
			//		gridCounter [allBoidsGPU [i].bin]++;

		}
	}

	public void CountingSort () {
		/*	string allB = "allBoidsGPU: \n";
		for (int i = 0; i < allBoidsGPU.Length; i++) {
			allB += "bin: " + allBoidsGPU [i].bin +
			" pos: " + allBoidsGPU [i].position +
			" vel: " + allBoidsGPU [i].velocity + "\n";
		}
		Debug.Log (allB);*/
		cumulativeCount = new int[gridCounter.Length];
		cumulativeCount [0] = gridCounter [0];

		//string sumCount = cumulativeCount [0] + " ";
		for (int i = 1; i < cumulativeCount.Length; i++) {
			cumulativeCount [i] = gridCounter [i] + cumulativeCount [i - 1]; 
			//sumCount += cumulativeCount [i] + " ";
		}
		//	string sortDeb = "";
		//	string unsortDeb = "";
		sortedBoids = new Boid[maxBoids];
		for (int i = allBoidsGPU.Length - 1; i > -1; i--) {
			//	Debug.Log ("Working on Boid: " + i + " Pos: " + allBoidsGPU [i].position + ", bin: " + allBoidsGPU [i].bin);
			//	Debug.Log ("cumulativeCount on that bin is: " + cumulativeCount [allBoidsGPU [i].bin] + " SumCount: " + sumCount);
			sortedBoids [(cumulativeCount [allBoidsGPU [i].bin]) - 1] = allBoidsGPU [i];
			cumulativeCount [allBoidsGPU [i].bin]--;

			//unsortDeb += allBoidsGPU [i].bin + " ";
		}


		/*for (int i = 0; i < sortedBoids.Length - 1; i++) {
			sortDeb += sortedBoids [i].bin + " ";
		}*/
		allBoidsGPU = sortedBoids; //Highly controversial line!!
		//	Debug.Log ("Sorted: " + sortDeb + "\nUnsorted: " + unsortDeb + "\nSumCount: " + sumCount);
	}

	public void SetPositions () {
		for (int i = 0; i < allBoidsGPU.Length; i++) {
			allBoids [i].position = allBoidsGPU [i].position;
			//		Debug.DrawLine (allBoids [i].transform.position, allBoids [i].transform.position + allBoidsGPU [i].velocity * 1, Color.green);
		}
	}

	public void Rotate () {
		for (int i = 0; i < allBoidsGPU.Length; i++) {
			allBoids [i].LookAt (allBoids [i].position + allBoidsGPU [i].velocity);
			allBoids [i].RotateAround (allBoids [i].position, allBoids [i].right, 90);
		}
	}
}
